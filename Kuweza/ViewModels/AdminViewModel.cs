﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kuweza.Models;
using System.Web;

namespace Kuweza.ViewModels
{

    public class AdminViewModel
    {
        public List<Customer> AllClients { get; set; }
        public List<Supplier> AllSuppliers { get; set; }
        //  public List<Job> AllJobs { get; set; }
        //  public List<Warehouse> AllWarehouses { get; set; }
        public List<Item> AllProducts { get; set; }
    }
}