﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Kuweza.Interfaces
{
    public interface IEmail
    {
        void SendMessage(string subject, MailAddress from, string html, List<string> recipients, string attachment = null);
    }
}