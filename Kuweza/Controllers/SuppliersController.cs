﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kuweza.Models;

namespace Kuweza.Controllers
{
    public class SuppliersController : Controller
    {
        private KuwezaContext db = new KuwezaContext();
        SageConnectBase validate = new SageConnectBase();
        // GET: Suppliers
        public ActionResult Index()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var suppliers = api.SupplierRequest.Get();
            return View(suppliers.Results);
        }
        // GET: Suppliers
        public ActionResult Update()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var suppliers = api.SupplierRequest.Get();
            return View(suppliers.Results);
        }
        //Ajax update customer
        public ActionResult UpdateSupplier(int ID, string CustomerName, string ContactName, string Tel, string Fax, string Mobile, string Email, string WebAddress, string SupplierCat, string DaCity, string DaCode, string DaPlace, string DaCountry, string PaCity, string PaCode, string PaPlace, string PaCountry, string BusReg)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var sup = api.SupplierRequest.Get(ID);
            sup.Name = CustomerName;
            sup.Fax = Fax;
            sup.Mobile = Mobile;
            sup.Email = Email;
            sup.WebAddress = WebAddress;
            sup.ContactName = ContactName;
            sup.DeliveryAddress01 = DaPlace;
            sup.DeliveryAddress03 = DaCity;
            sup.DeliveryAddress04 = DaCountry;
            sup.DeliveryAddress05 = DaCode;
            sup.PostalAddress01 = PaPlace;
            sup.PostalAddress03 = PaCity;
            sup.PostalAddress04 = PaCountry;
            sup.PostalAddress05 = PaCode;
            sup.BusinessRegistrationNumber = BusReg;
            sup.Telephone = Tel;
            api.SupplierRequest.Save(sup);
            return null;
        }
        public ActionResult GetCompanyList()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            List<Supplier> allItems = api.SupplierRequest.Get().Results;
            string listItems = "";
            foreach (Supplier items in allItems)
            {
                listItems += String.Format("<div class='row' id={0} draggable='true' ondragstart='dragstart(event)' onclick='editSupplier(this.id)'>{1}</div>", items.ID, items.Name);
            }
            string data = String.Format(@"{0}", listItems);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSuppliers(string search)
        {
           // string nm = "Craig Baker Company";
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            List<Supplier> allItems = api.SupplierRequest.Get().Results.ToList();
            string listItems = "";
            int x = 0;
            foreach (Supplier items in allItems)
            {
                listItems += String.Format("<div class='card darken-1' id='card-{1}'><div class='card-content black-text'><span class='card-title' style='color:black'><strong>{0}</strong></span><div id = 'dvright-{1}' class='dvmainrigth'><ul id = 'lstselectedproducts-{1}'></ ul ></div><br/><input type='button' class='alert-info glyphicon glyphicon-plus-sign btn-large' id='btn-{1}' name='btn-{1}' value='Review & Send Quote'></div></div>",items.Name, Convert.ToString(++x));
            }
            string data = String.Format(@"{0}", listItems);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SearchSuppliers(string search)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            List<Supplier> allItems = api.SupplierRequest.Get().Results.Where(i => i.Name.Contains(search)).ToList();
            string listItems = "";
            int x = 0;
            foreach (Supplier items in allItems)
            {
                listItems += String.Format("<div class='card darken-1' id='card-{1}'><div class='card-content black-text'><span class='card-title' style='color:black'><strong>{0}</strong></span><div id = 'dvright-{1}' class='dvmainrigth'><ul id = 'lstselectedproducts-{1}'></ ul ></div><br/><input type='button' class='alert-info glyphicon glyphicon-plus-sign btn-large' id='btn-{1}' name='btn-{1}' value='Review & Send Quote'></div></div>", items.Name, Convert.ToString(++x));
            }
            string data = String.Format(@"{0}", listItems);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Ajax delete supplier
        public ActionResult DeleteSupplier(int SupplierId)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId); ;
            api.SupplierRequest.Delete(SupplierId);
            return null;
        }
        //Ajax call to edit customer
        public ActionResult EditSupplier(int SupplierId)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var suppliers = api.SupplierRequest.Get(SupplierId);
            string data = String.Format(@"<div class='col s12 m12 l12'>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Supplier Name</div>
                    <div class='col-lg-6'><input type='text' id='sname' name='sname' value='{0}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Contact Name</div>
                    <div class='col-lg-6'><input type='text' id='contactname' name='contactname' value='{1}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Telephone #</div>
                    <div class='col-lg-6'><input type='text' id='tel' name='tel' value='{2}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Fax #</div>
                    <div class='col-lg-6'><input type='text' id='fax' name='fax' value='{3}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Mobile #</div>
                    <div class='col-lg-6'><input type='text' id='mobile' name='mobile' value='{4}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Email Address</div>
                    <div class='col-lg-6'><input type='text' id='email' name='email' value='{5}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Web Address</div>
                    <div class='col-lg-6'><input type='text' id='webaddress' name='webaddress' value='{6}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Supplier Category</div>
                    <div class='col-lg-6'><input type='text' id='scat' name='scat' value='{7}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Postal Address Place</div>
                    <div class='col-lg-6'><input type='text' id='pap' name='pap' value='{8}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Postal Address City</div>
                    <div class='col-lg-6'><input type='text' id='pac' name='pac' value='{9}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Postal Address Country</div>
                    <div class='col-lg-6'><input type='text' id='pacountry' name='pacountry' value='{10}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Postal Address Code</div>
                    <div class='col-lg-6'><input type='text' id='pacode' name='pacode' value='{11}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Delivery Address Place</div>
                    <div class='col-lg-6'><input type='text' id='dap' name='dap' value='{12}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Delivery Address City</div>
                    <div class='col-lg-6'><input type='text' id='dac' name='dac' value='{13}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Delivery Address Country</div>
                    <div class='col-lg-6'><input type='text' id='dacountry' name='dacountry' value='{14}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Delivery Address Area Code</div>
                    <div class='col-lg-6'><input type='text' id='dacode' name='dacode' value='{15}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>Business Reg#</div>
                    <div class='col-lg-6'><input type='text' id='busreg' name='busreg' value='{16}' /></div>
                </div>
                <div class='row'>
                    <div class='col-lg-6' align='left'>&nbsp;</div>
                    <div class='col-lg-6'><input type='button' id='btnsubmit' name='btnsubmit' value='Update Supplier' onclick='updateSupplier({17})'/></div>
                </div>
   </div>", suppliers.Name, suppliers.ContactName, suppliers.Telephone, suppliers.Fax, suppliers.Mobile, suppliers.Email, suppliers.WebAddress, suppliers.TextField1, suppliers.PostalAddress01, suppliers.PostalAddress03, suppliers.PostalAddress04, suppliers.PostalAddress05, suppliers.DeliveryAddress01, suppliers.DeliveryAddress03, suppliers.DeliveryAddress04, suppliers.DeliveryAddress05, suppliers.BusinessRegistrationNumber, suppliers.ID);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Ajax add supplier
        public ActionResult AddSupplier(string SupplierName, string TaxRef, string ContactName, string Tel, string Fax, string Mobile, string Email, string WebAddress, string SupplierCat, string DaCity, string DaCode, string DaPlace, string DaCountry, string PaCity, string PaCode, string PaPlace, string PaCountry, string BusReg)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var sup = new Supplier
            {
                Name = SupplierName,
                TaxReference = TaxRef,
                Fax = Fax,
                Mobile = Mobile,
                Email = Email,
                WebAddress = WebAddress,
                ContactName = ContactName,
                DeliveryAddress01 = DaPlace,
                DeliveryAddress03 = DaCity,
                DeliveryAddress04 = DaCountry,
                DeliveryAddress05 = DaCode,
                PostalAddress01 = PaPlace,
                PostalAddress03 = PaCity,
                PostalAddress04 = PaCountry,
                PostalAddress05 = PaCode,
                BusinessRegistrationNumber = BusReg,
                Telephone = Tel
            };
            api.SupplierRequest.Save(sup);
            return null;
        }

        // GET: Suppliers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Supplier supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            return View(supplier);
        }

        // GET: Suppliers/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.Categories.ToList(), "CategoryId", "Description");
            return View();
        }

        // POST: Suppliers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,TaxReference,ContactName,Telephone,Fax,Mobile,Email,WebAddress,PostalAddress01,PostalAddress02,PostalAddress03,PostalAddress04,PostalAddress05,DeliveryAddress01,DeliveryAddress02,DeliveryAddress03,DeliveryAddress04,DeliveryAddress05,BusinessRegistrationNumber,RMCDApprovalNumber,TextField1")] Supplier supplier)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var sup = new Supplier
            {
                Name = supplier.Name,
                TaxReference=supplier.TaxReference,
                Fax = supplier.Fax,
                Mobile = supplier.Mobile,
                Email = supplier.Email,
                WebAddress = supplier.WebAddress,
                ContactName = supplier.ContactName,
                DeliveryAddress01 = supplier.DeliveryAddress01,
                DeliveryAddress02 = supplier.DeliveryAddress02,
                DeliveryAddress03 = supplier.DeliveryAddress03,
                DeliveryAddress04 = supplier.DeliveryAddress04,
                DeliveryAddress05 = supplier.DeliveryAddress05,
                PostalAddress01 = supplier.PostalAddress01,
                PostalAddress02 = supplier.PostalAddress02,
                PostalAddress03 = supplier.PostalAddress03,
                PostalAddress04 = supplier.PostalAddress04,
                PostalAddress05 = supplier.PostalAddress05,
                BusinessRegistrationNumber=supplier.BusinessRegistrationNumber,
                RMCDApprovalNumber=supplier.RMCDApprovalNumber,
                Telephone = supplier.Telephone,  
                TextField1=supplier.TextField1       
            };
            api.SupplierRequest.Save(sup);
            return RedirectToAction("Index");
        }

        // GET: Suppliers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Supplier supplier = db.Suppliers.Find(id);
            if (supplier == null)
            {
                return HttpNotFound();
            }
            return View(supplier);
        }

        // POST: Suppliers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,TaxReference,ContactName,Telephone,Fax,Mobile,Email,WebAddress,PostalAddress01,PostalAddress02,PostalAddress03,PostalAddress04,PostalAddress05,DeliveryAddress01,DeliveryAddress02,DeliveryAddress03,DeliveryAddress04,DeliveryAddress05,BusinessRegistrationNumber,RMCDApprovalNumber, TextField1")] Supplier supplier)
        {
            if (ModelState.IsValid)
            {
                db.Entry(supplier).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(supplier);
        }

        // GET: Suppliers/Delete/5
        public ActionResult Delete(int id)
        {
          //  int supplierId = id;
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var supplier = api.SupplierRequest.Delete(id);
            return RedirectToAction("Index");
        }

        // POST: Suppliers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //int supplierId = id;
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var supplier = api.SupplierRequest.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
