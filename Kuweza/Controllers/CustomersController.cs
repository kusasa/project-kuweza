﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kuweza.Models;

namespace Kuweza.Controllers
{
    public class CustomersController : Controller
    {
        private KuwezaContext db = new KuwezaContext();
        //Initialising connection to sage. This will check the username, password, api key and company id
        SageConnectBase validate = new SageConnectBase();
        //GET: Customers
        public ActionResult Index()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var cust = api.CustomerRequest.Get();
            return View(cust.Results);
        }
        //UPDATE: Customers
        public ActionResult Update()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var cust = api.CustomerRequest.Get();
            return View(cust.Results);
        }
        public ActionResult GetCompanyList()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            List<Customer> allItems = api.CustomerRequest.Get().Results;
            string listItems = "";
            foreach (Customer items in allItems)
            {
                listItems += String.Format("<div class='row' id={0} draggable='true' ondragstart='dragstart(event)' onclick='editCustomer(this.id)' style='cursor:pointer;'>{1}</div>", items.ID, items.Name);
            }
            string data = String.Format(@"{0}", listItems);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Ajax call to edit customer
        public ActionResult EditCustomer(int CustomerId)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var customers = api.CustomerRequest.Get(CustomerId);       
            string data = String.Format(@"<div class='col s12 m12 l12'>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Customer Name</div>
                    <div class='col m6 l6'><input type='text' id='cname' name='cname' value='{0}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Contact Name</div>
                    <div class='col m6 l6'><input type='text' id='contactname' name='contactname' value='{1}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Telephone #</div>
                    <div class='col m6 l6'><input type='text' id='tel' name='tel' value='{2}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Fax #</div>
                    <div class='col m6 l6'><input type='text' id='fax' name='fax' value='{3}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Mobile #</div>
                    <div class='col m6 l6'><input type='text' id='mobile' name='mobile' value={4} /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Email Address</div>
                    <div class='col m6 l6'><input type='text' id='email' name='email' value='{5}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Web Address</div>
                    <div class='col m6 l6'><input type='text' id='webaddress' name='webaddress' value='{6}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Supplier Category</div>
                    <div class='col m6 l6'><input type='text' id='scat' name='scat' value='{7}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Postal Address Place</div>
                    <div class='col m6 l6'><input type='text' id='pap' name='pap' value='{8}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Postal Address City</div>
                    <div class='col m6 l6'><input type='text' id='pac' name='pac' value='{9}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Postal Address Country</div>
                    <div class='col m6 l6'><input type='text' id='pacountry' name='pacountry' value='{10}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Postal Address Code</div>
                    <div class='col m6 l6'><input type='text' id='pacode' name='pacode' value='{11}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Delivery Address Place</div>
                    <div class='col m6 l6'><input type='text' id='dap' name='dap' value='{12}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Delivery Address City</div>
                    <div class='col m6 l6'><input type='text' id='dac' name='dac' value='{13}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Delivery Address Country</div>
                    <div class='col m6 l6'><input type='text' id='dacountry' name='dacountry' value='{14}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Delivery Address Area Code</div>
                    <div class='col m6 l6'><input type='text' id='dacode' name='dacode' value='{15}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Business Reg#</div>
                    <div class='col m6 l6'><input type='text' id='busreg' name='busreg' value='{16}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>Mark Up(%)</div>
                    <div class='col m6 l6'><input type='text' id='markup' name='markup' value='{17}' /></div>
                </div>
                <div class='row'>
                    <div class='col m6 l6' align='left'>&nbsp;</div>
                    <div class='col m6 l6'><input type='button' id='btnsubmit' name='btnsubmit' value='Update Customer' onclick='updateCustomer({18})' /></div>
                </div>
   </div>", customers.Name, customers.ContactName, customers.Telephone, customers.Fax, customers.Mobile, customers.Email, customers.WebAddress, customers.TextField1, customers.PostalAddress01, customers.PostalAddress03, customers.PostalAddress04, customers.PostalAddress05, customers.DeliveryAddress01, customers.DeliveryAddress03, customers.DeliveryAddress04, customers.DeliveryAddress05, customers.BusinessRegistrationNumber, customers.TextField2, customers.ID);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Ajax delete customer
        public ActionResult DeleteCustomer(int CustomerId)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId); ;
            var result = api.CustomerRequest.Delete(CustomerId);
            return null;
        }
        //Ajax add supplier
        public ActionResult AddCustomer(string CustomerName, string ContactName, string Tel, string Fax, string Mobile, string Email, string WebAddress, string SupplierCat, string DaCity, string DaCode, string DaPlace, string DaCountry, string PaCity, string PaCode, string PaPlace, string PaCountry, string BusReg, string MarkUp)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var cust = new Customer
            {
                Name = CustomerName,
                Fax = Fax,
                Mobile = Mobile,
                Email = Email,
                WebAddress = WebAddress,
                ContactName = ContactName,
                DeliveryAddress01 = DaPlace,
                DeliveryAddress03 = DaCity,
                DeliveryAddress04 = DaCountry,
                DeliveryAddress05 = DaCode,
                PostalAddress01 = PaPlace,
                PostalAddress03 = PaCity,
                PostalAddress04 = PaCountry,
                PostalAddress05 = PaCode,
                BusinessRegistrationNumber = BusReg,
                TextField1 = SupplierCat,
                TextField2 = MarkUp,
                Telephone = Tel
            };
            api.CustomerRequest.Save(cust);
            return null;
        }
        //Ajax update customer
        public ActionResult UpdateCustomer(int ID, string CustomerName, string ContactName, string Tel, string Fax, string Mobile, string Email, string WebAddress, string SupplierCat, string DaCity, string DaCode, string DaPlace, string DaCountry, string PaCity, string PaCode, string PaPlace, string PaCountry, string BusReg, string MarkUp)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var cust = api.CustomerRequest.Get(ID);
            cust.Name = CustomerName;
            cust.Fax = Fax;
            cust.Mobile = Mobile;
            cust.Email = Email;
            cust.WebAddress = WebAddress;
            cust.ContactName = ContactName;
            cust.DeliveryAddress01 = DaPlace;
            cust.DeliveryAddress03 = DaCity;
            cust.DeliveryAddress04 = DaCountry;
            cust.DeliveryAddress05 = DaCode;
            cust.PostalAddress01 = PaPlace;
            cust.PostalAddress03 = PaCity;
            cust.PostalAddress04 = PaCountry;
            cust.PostalAddress05 = PaCode;
            cust.BusinessRegistrationNumber = BusReg;
            cust.Telephone = Tel;
            cust.TextField2 = MarkUp;
            api.CustomerRequest.Save(cust);
            return null;
        }

        public ActionResult GetAll()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var cust = api.CustomerRequest.Get();
            return View(cust.Results);
        }

        // GET: Customers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: Customers/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.Categories.ToList(), "CategoryId", "Description");
            return View();
        }

        // POST: Customers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,ContactName,Telephone,Fax,Mobile,Email,WebAddress,BusinessRegistrationNumber,PostalAddress01,PostalAddress02,PostalAddress03,PostalAddress04,PostalAddress05,DeliveryAddress01,DeliveryAddress02,DeliveryAddress03,DeliveryAddress04,DeliveryAddress05,SalesRepresentativeId,TextField1")] Customer customer)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var client = new Customer
                {
                    Name = customer.Name,                 
                    ContactName = customer.ContactName,                
                    Fax = customer.Fax,
                    Mobile=customer.Mobile,
                    Email=customer.Email,
                    WebAddress=customer.WebAddress,
                    DeliveryAddress01 = customer.DeliveryAddress01,
                    DeliveryAddress02 = customer.DeliveryAddress02,
                    DeliveryAddress03 = customer.DeliveryAddress03,
                    DeliveryAddress04 = customer.DeliveryAddress04,
                    DeliveryAddress05 = customer.DeliveryAddress05,                
                    PostalAddress01 = customer.PostalAddress01,
                    PostalAddress02 = customer.PostalAddress02,
                    PostalAddress03 = customer.PostalAddress03,
                    PostalAddress04 = customer.PostalAddress04,
                    PostalAddress05 = customer.PostalAddress05,
                    BusinessRegistrationNumber=customer.BusinessRegistrationNumber,
                    Telephone = customer.Telephone,
                    TextField1=customer.TextField1,
                    SalesRepresentativeId = null
                };
                var cust = api.CustomerRequest.Save(client);
                return RedirectToAction("Index");
        }

        // GET: Customers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = db.Customers.Find(id);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // POST: Customers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,ContactName,Telephone,Fax,Mobile,Email,WebAddress,BusinessRegistrationNumber,PostalAddress01,PostalAddress02,PostalAddress03,PostalAddress04,PostalAddress05,DeliveryAddress01,DeliveryAddress02,DeliveryAddress03,DeliveryAddress04,DeliveryAddress05,SalesRepresentativeId,TextField1")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(customer);
        }

        // GET: Customers/Delete/5
        public ActionResult Delete(int id)
        {
            int customerId = id;
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId); ;
            var result = api.CustomerRequest.Delete(customerId);
            return RedirectToAction("Index");
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            int customerId = id;
            var result = api.CustomerRequest.Delete(customerId);
            var cust = api.CustomerRequest.Get();
            return View(cust.Results);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}    