﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kuweza.Models;

namespace Kuweza.Controllers
{
    public class ItemsController : Controller
    {
        private KuwezaContext db = new KuwezaContext();
        SageConnectBase validate = new SageConnectBase();
        // GET: Items
       public ActionResult Index()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var items = api.ItemRequest.Get();
            return View(items.Results);
         }
        public ActionResult Update()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var items = api.ItemRequest.Get();
            return View(items.Results);
        }
        public ActionResult GetItemList()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            List<Item> allItems = api.ItemRequest.Get().Results;
            string listItems = "";
            foreach (Item items in allItems)
            {
                listItems += String.Format("<div class='row' id={0} draggable='true' ondragstart='dragstart(event)' onclick='editItem(this.id)'>{1}</div>", items.ID, items.Description);
            }
            string data = String.Format(@"{0}", listItems);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetInitialItemList()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            List<Item> allItems = api.ItemRequest.Get().Results;
            string listItems = "";
            foreach (Item items in allItems)
            {
                listItems += String.Format("<div class='row' id={0} draggable='true' ondragstart='dragstart(event)' ondrag='dragitem(event, this.id)'>{1}</div>", items.ID, items.Description);
            }
            string data = String.Format(@"{0}", listItems);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Ajax delete supplier
        public ActionResult DeleteItem(int ItemId)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId); ;
            api.ItemRequest.Delete(ItemId);
            return null;
        }
        //Ajax call to edit customer
        public ActionResult EditItem(int ItemId)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var items = api.ItemRequest.Get(ItemId);
            string data = String.Format(@"<div class='col s12 m12 l12' >
             <div class='row' > <div class='col m12 l12' align='left' > Item Code</div > <div class='col m12 l12' > <input type='text' id='pcode' name='pcode' value='{0}' /> </div > </div >
              <div class='row' > <div class='col m12 l12' align='left' > Description</div > <div class='col m12 l12' > <input type='text' id='pdescription' name='pdescription' value='{1}' /> </div > </div >
              <div class='row' > <div class='col m12 l12' align='left' > Price Exclusive</div > <div class='col m12 l12' > <input type='text' id='price-ex' name='price-ex' value='{2}' /> </div > </div > 
              <div class='row' > <div class='col m12 l12' align='left' > Price Inclusive</div > <div class='col m12 l12' > <input type='text' id='price-inc' name='price-inc' value='{3}' /> </div > </div >
              <div class='row' > <div class='col m12 l12' align='left' > Category</div > <div class='col m12 l12' > <input type='text' id='pcat' name='pcat' value='{4}' /> </div > </div >
              <div class='row' > <div class='col m12 l12' align='left' > &nbsp;</div > <div class='col m12 l12' > <input type='button' id='btnsubmit' name='btnsubmit' value='Update Item' onclick='updateItem({5})' /> </div > </div >
     </div >", items.Code, items.Description, items.PriceExclusive, items.PriceInclusive, items.TextUserField2, items.ID);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AllProducts()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var items = api.ItemRequest.Get();
            return View(items.Results);
        }
        public ActionResult AddItem(string ItemCode, string ItemName, decimal CostInclusive, decimal CostExclusive, string ItemCategory)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var products = new Item
            {
                Code = ItemCode,
                Description = ItemName,
                PriceExclusive = CostExclusive,
                Active = true,
                PriceInclusive = CostInclusive,
                TextUserField2 = ItemCategory
            };
            api.ItemRequest.Save(products);
            return null;
        }
        public ActionResult UpdateItem(int ID, string ItemCode, string ItemName, decimal CostInclusive, decimal CostExclusive, string ItemCategory)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var items = api.ItemRequest.Get(ID);
            items.Code = ItemCode;
            items.Description = ItemName;
            items.PriceExclusive = CostExclusive;
            items.Active = true;
            items.PriceInclusive = CostInclusive;
            items.TextUserField2 = ItemCategory;
            api.ItemRequest.Save(items);
            return null;
        }


        public ActionResult ListItems(string sLookUP)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var items = api.ItemRequest.Get();
            return View(items.Results);
        }


        // GET: Items/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // GET: Items/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = new SelectList(db.Categories.ToList(), "CategoryId", "Description");
            return View();
        }

        // POST: Items/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Code,Description,PriceInclusive,PriceExclusive,TextUserField1,NumericUserField1,NumericUserField2,NumericUserField3,TextUserField2")] Item item)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var products = new Item
            { 
                Code = item.Code,
                Description = item.Description,
                PriceExclusive=item.PriceExclusive,
                Active=true,
                PriceInclusive=item.PriceInclusive,
                TextUserField1=item.TextUserField1,
                NumericUserField1=item.NumericUserField1,
                NumericUserField2=item.NumericUserField2,
                NumericUserField3=item.NumericUserField3,
                TextUserField2=item.TextUserField2               
            };
         api.ItemRequest.Save(products);
         return RedirectToAction("Index");
        }

        // GET: Items/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Item item = db.Items.Find(id);
            if (item == null)
            {
                return HttpNotFound();
            }
            return View(item);
        }

        // POST: Items/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Code,Description,AverageCost")] Item item)
        {
            if (ModelState.IsValid)
            {
                db.Entry(item).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(item);
        }

        // GET: Items/Delete/5
        public ActionResult Delete(int id)
        {
            int itemId = id;
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var result = api.ItemRequest.Delete(itemId);
            return RedirectToAction("Index");
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Item item = db.Items.Find(id);
            db.Items.Remove(item);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
