﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kuweza.Models;
using System.Web.Mvc;

namespace Kuweza.Controllers
{
    public class ProductsController : Controller
    {
        SageConnectBase validate = new SageConnectBase();
        private KuwezaContext db = new KuwezaContext();
        // GET: Product
        public ActionResult Index()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var suppliers = api.SupplierRequest.Get();
           
            return View(suppliers.Results);
        }

        public ActionResult Supp()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var suppliers = api.SupplierRequest.Get();
            return View(suppliers.Results);
        }
        public ActionResult GetItems(string JobNumber)
        {
            List<Product> allItems = db.Products.Where(i => i.JobNumber==JobNumber).ToList();
            string listItems = "";
            foreach (Product items in allItems)
            {
                listItems += String.Format("<li id={0} draggable='true' ondragstart='dragstart(event, this.id)' class='badge list-products'>{1}:{2}</li>", items.SageProductId, items.ProductName, items.Quantity);
            }
            string data = String.Format(@"<div class='row alert alert-info'>
                <div class='col-lg-12'><b><u>Product:Quantity</u></b></div>
                </div>            
                {0}", listItems);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Add Item From  Ajax Call During Drag And Drop
        public ActionResult AddItem(string ProductName, string JobNumber, int ClientId, string RandomId, int SageProductId)
        {

            Product itemsList = new Product();
            Product item = db.Products.FirstOrDefault(i => i.JobNumber== JobNumber && i.SageProductId==SageProductId );
            if (item == null)
            {
                itemsList.JobNumber = JobNumber;
                itemsList.ProductName = ProductName;
                itemsList.ClientId = ClientId;
                itemsList.RandomId = RandomId;
                itemsList.SageProductId = SageProductId;
                db.Products.Add(itemsList);
                db.SaveChanges();
            }
            return null;
        }
        //Delete item from Ajax Call During Drag And Drop
        public ActionResult DeleteItem(string RandomID)
        {
            Product item = db.Products.FirstOrDefault(i => i.RandomId == RandomID);
            db.Products.Remove(item);
            db.SaveChanges();
            return null;
        }
    }
}