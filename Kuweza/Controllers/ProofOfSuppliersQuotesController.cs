﻿using Kuweza.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Kuweza.Controllers
{
    public class ProofOfSuppliersQuotesController : Controller
    {
        private KuwezaContext db = new KuwezaContext();
        // GET: ProofOfSuppliersQuotes
        public ActionResult Index()
        {
            return View();
        }
        public FileResult Download(string fileName)
        {
            return File("~/SuppliersProofOfRequest/" + fileName, System.Net.Mime.MediaTypeNames.Application.Octet);
        }
        public ActionResult ProofOfEnquiry(string fileName)
        {
            return Download(fileName);
        }
        [HttpGet]
        public FileResult DownloadDataFile(string fileName)
        {
            using (var mem = new MemoryStream())
            {
                // Create spreadsheet based on widgetId...
                // Or get the path for a file on the server...
                return File(mem, "~/SuppliersProofOfRequest/" + fileName, System.Net.Mime.MediaTypeNames.Application.Octet);
            }
        }
        [HttpPost]

        public ActionResult DownloadFile(string JobNumber, int CompanyId)
        {
            string fileName=null;
            ProofOfSuppliersQuote item = db.ProofOfSuppliersQuotes.FirstOrDefault(i => i.JobNumber == JobNumber);
            fileName = item.FileName;
            if (fileName == null)
            {
               return Json("No file to download! Upload a file first");
            }
            else
            {
               return DownloadDataFile(fileName);
            }
           // return Json("Your file has been downloaded");
        }   

        [HttpPost]
        public ActionResult UploadFiles(string JobNumber, int CompanyId, string OtherNotes)
        {
            // Checking if file already exist or not. If exist create if not update  
            ProofOfSuppliersQuote quoteDetails = new ProofOfSuppliersQuote();
            ProofOfSuppliersQuote item = db.ProofOfSuppliersQuotes.FirstOrDefault(m => m.JobNumber == JobNumber && m.SupplierId == CompanyId);
            if (Request.Files.Count > 0)
            {            
                try
                {
                    //  Get all files from Request object  
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        //string path = AppDomain.CurrentDomain.BaseDirectory + "Uploads/";  
                        //string filename = Path.GetFileName(Request.Files[i].FileName);  
                        HttpPostedFileBase file = files[i];
                        string fname;
                        // Checking for Internet Explorer  
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = JobNumber + CompanyId + "ID" + testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            fname = JobNumber + "ID" + CompanyId + file.FileName;
                        }
                        // Get the complete folder path and store the file inside it.  
                        string fName= Path.Combine(Server.MapPath("~/SuppliersProofOfRequest/"), fname);
                        file.SaveAs(fName);
                        try
                        {
                            if (item == null)
                            {
                                //Create Details
                                quoteDetails.JobNumber = JobNumber;
                                quoteDetails.SupplierId = CompanyId;
                                quoteDetails.OtherInfor = OtherNotes;
                                quoteDetails.FileName = fname;
                                db.ProofOfSuppliersQuotes.Add(quoteDetails);
                                db.SaveChanges();
                            }
                            else
                            if (item != null)
                            {
                                //Update Details
                                item.JobNumber = JobNumber;
                                item.SupplierId = CompanyId;
                                item.OtherInfor = OtherNotes;
                                item.FileName = fname;
                                db.SaveChanges();
                            }
                        }
                        catch (Exception ex)
                        {
                            return Json("Error! " + ex);

                        }
                    }
                    return Json("File Uploaded Successfully! ");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }         
            }
            else
            {
                return Json("No files selected.");
            }

        }

    }
   
}