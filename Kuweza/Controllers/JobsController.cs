﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kuweza.Models;
using System.IO;

namespace Kuweza.Controllers
{
    public class JobsController : Controller
    {
        private KuwezaContext db = new KuwezaContext();
        SageConnectBase validate = new SageConnectBase();

        // GET: Jobs
        public ActionResult Index()
        {
            return View(db.Jobs.ToList());
        }

        // GET: Jobs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return HttpNotFound();
            }
            return View(job);
        }

        // GET: Jobs/Create
        public ActionResult Create()
        {
            ViewBag.UserName = new SelectList(db.Users.ToList(), "UserName", "UserName");
            return View();
        }

        // POST: Jobs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "JobId,JobNumber,StatusId,TypeOfEquiry,Quoted,DateReceived,KuwezaQuoteNo,MethodOfCommunication,AssignTo,ClientId,UserId")] Job job, HttpPostedFileBase file, int id)
        {
            if (ModelState.IsValid)
            {
                Job newjob = new Job
                {
                    DateReceived = DateTime.Today,
                    MethodOfCommunication = job.MethodOfCommunication,
                    TypeOfEquiry = job.TypeOfEquiry,
                    KuwezaQuoteNo = "KQ128020",
                    StatusId = 0,
                    AssignTo = job.AssignTo,
                   // ProofOfEnquiry = job.ProofOfEnquiry,
                    ClientId = id,
                    Quoted = false,
                    UserId = "100500"
                };
                int month = DateTime.Now.Month;
                int year = DateTime.Now.Year;
                int day = DateTime.Now.Day;
                int hour = DateTime.Now.Hour;
                int minute = DateTime.Now.Minute;
                string typ = newjob.TypeOfEquiry.Substring(0,1);
                string jobNum = newjob.ClientId + "-" + typ + "-" + year + month + day + "-" + hour + minute;
                newjob.JobNumber = jobNum;

                if (Request != null)
                {
                        file = Request.Files["EnquiryUpload"];
                        string fileName = file.FileName;                                   
                        string fileContentType = file.ContentType;
                        byte[] fileBytes = new byte[file.ContentLength];
                        file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                        file.SaveAs((Path.Combine(Server.MapPath("~/OrderRequestFiles/"), fileName)));
                        newjob.ProofOfEnquiry = fileName;                  
                }

                db.Jobs.Add(newjob);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(job);
        }

        // GET: Jobs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return HttpNotFound();
            }
            return View(job);
        }

        // POST: Jobs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "JobId,JobNumber,StatusId,TypeOfEquiry,Quoted,DateReceived,KuwezaQuoteNo,ProofOfEnquiry,MethodOfCommunication,AssignTo,ClientId,UserId")] Job job)
        {
            if (ModelState.IsValid)
            {
                db.Entry(job).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(job);
        }

        // GET: Jobs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Job job = db.Jobs.Find(id);
            if (job == null)
            {
                return HttpNotFound();
            }
            return View(job);
        }

        // POST: Jobs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Job job = db.Jobs.Find(id);
            db.Jobs.Remove(job);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public FileResult Download(string fileName)
        {
            return File("~/OrderRequestFiles/" +fileName, System.Net.Mime.MediaTypeNames.Application.Octet);
        }
        public ActionResult ProofOfEnquiry(string fileName)
        {
            return Download(fileName);
        }
        public ActionResult CreateList()
        {
            return View();
        }
       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
