﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kuweza.Interfaces;
using Kuweza.Models;
using System.Web.Mvc;
using System.Net.Mail;
using Spire.Doc;

namespace Kuweza.Controllers
{
    public class ReportEmailerController : Controller
    {
        // GET: ReportEmailer
        public ActionResult Index()
        {
            return View();
        }
        private KuwezaContext db = new KuwezaContext();
        SageConnectBase validate = new SageConnectBase();

        private IEmail _emailSender;

        public ReportEmailerController(IEmail emailSender)
        {
            _emailSender = emailSender;
        }
      //  [OverrideAuthorization]
        public ActionResult Do(string MainEmail, string CCEmail, string JobNumber, int SupplierId, string CustomMessage )
        {
            string doc = JobNumber + "C" + SupplierId + "Quote.docx";
            string messageSubject = "Items Requests(Kuweza)";
            //Message body
            string body = String.Format(@"<html>
                <head>
               <b>SEE ATTACHED FILE FOR THE QUOTE</b>
                </head>              
                <body>
                <p>{0}</p>
                <p>[Thank you for doing business with us]</p>
                <tbody></html>", CustomMessage);
           //Send attachment
            try
            {
                string docPath = Server.MapPath("~/Quotes_To_Suppliers/"+ doc);
                if (CCEmail == null | CCEmail == "")
                {

                    var recipients = new List<string>();
                    recipients.Add(@"Supplier<" + MainEmail + ">");
                    var fromAddress = new MailAddress("salebot@saleboat.co.za", "Items List From Kuweza");
                    _emailSender.SendMessage(messageSubject, fromAddress, body, recipients, docPath);
                }
                else
                {
                    var recipients = new List<string>();
                    recipients.Add(@"Supplier<" + MainEmail + ">");
                    recipients.Add(@"Supplier<" + CCEmail + ">");
                    var fromAddress = new MailAddress("salebot@saleboat.co.za", "Items List From Kuweza");
                    _emailSender.SendMessage(messageSubject, fromAddress, body, recipients, docPath);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        //  [OverrideAuthorization]
        public ActionResult SendPdf(string MainEmail, string CCEmail, string JobNumber, int SupplierId, string CustomMessage)
        {
            string doc = JobNumber + "C" + SupplierId + "Quote.docx";
            string doc2 = JobNumber + "C" + SupplierId + "Quote.pdf";
            //Load Document  
            Document convertdoc = new Document();
            //Pass path of Word Document in LoadFromFile method  
            convertdoc.LoadFromFile(Server.MapPath("~/Quotes_To_Suppliers/"+ doc));
            //Pass Document Name and FileFormat of Document as Parameter in SaveToFile Method  
            convertdoc.SaveToFile(Server.MapPath("~/Quotes_To_Suppliers/"+ doc2), FileFormat.PDF);
            string messageSubject = "Items Requests(Kuweza)";
            string body = String.Format(@"<html>
                 <head>
               <b>SEE ATTACHED FILE FOR THE QUOTE</b>
                </head>              
                <body>
                <p>{0}</p>
                <p>Thank you for doing business with us</p>
                <tbody>
                ", CustomMessage);
            //Send attachment
            try
            {
                string docPath = Server.MapPath("~/Quotes_To_Suppliers/"+ doc2);
                if (CCEmail == null | CCEmail == "")
                {

                    var recipients = new List<string>();
                    recipients.Add(@"Supplier<" + MainEmail + ">");
                    var fromAddress = new MailAddress("salebot@saleboat.co.za", "Items List From Kuweza");
                    _emailSender.SendMessage(messageSubject, fromAddress, body, recipients, docPath);
                }
                else
                {
                    var recipients = new List<string>();
                    recipients.Add(@"Supplier<" + MainEmail + ">");
                    recipients.Add(@"Supplier<" + CCEmail + ">");
                    var fromAddress = new MailAddress("salebot@saleboat.co.za", "Items List From Kuweza");
                    _emailSender.SendMessage(messageSubject, fromAddress, body, recipients, docPath);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        public ActionResult MailFinalQuote(string JobNumber, int CustomerId, string CustomMessage, string CcEmail)
        {
            //get the emails of supplier//
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var customer = api.CustomerRequest.Get(CustomerId);
            var MainEmail = customer.Email;
            var CCEmail = CcEmail;
            var CompanyName = customer.Name;
            string doc = JobNumber + "C" + CustomerId + "Invoice.docx";
            string messageSubject = "Quote Items(Kuweza)";
            string body = String.Format(@"<html>
                 <head>
               <b>SEE ATTACHED FILE FOR THE QUOTE</b>
                </head>               
                <body>
                <p>{0}</p>
                <p>[Thank you for doing business with us]</p>
                <tbody>
                ", CustomMessage);
            try
            {
                string docPath = Server.MapPath("~/Invoices_To_Customers/"+ doc);
                if (CCEmail == null | CCEmail == "")
                {

                    var recipients = new List<string>();
                    recipients.Add(@"Customer<" + MainEmail + ">");
                    var fromAddress = new MailAddress("salebot@saleboat.co.za", "Items List From Kuweza");
                    _emailSender.SendMessage(messageSubject, fromAddress, body, recipients, docPath);
                }
                else
                {
                    var recipients = new List<string>();
                    recipients.Add(@"Customer<" + MainEmail + ">");
                    recipients.Add(@"Customer<" + CCEmail + ">");
                    var fromAddress = new MailAddress("salebot@saleboat.co.za", "Items List From Kuweza");
                    _emailSender.SendMessage(messageSubject, fromAddress, body, recipients, docPath);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
        public ActionResult MailFinalQuotePdf(string JobNumber, int CustomerId, string CustomMessage, string CcEmail)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            var customer = api.CustomerRequest.Get(CustomerId);
            var MainEmail = customer.Email;
            var CCEmail = CcEmail;
            var CompanyName = customer.Name;
            string doc = JobNumber + "C" + CustomerId + "Invoice.docx";
            string doc2 = JobNumber + "C" + CustomerId + "Invoice.pdf";
            //Load Document  
            Document convertdoc = new Document();
            //Pass path of Word Document in LoadFromFile method  
            convertdoc.LoadFromFile(Server.MapPath("~/Invoices_To_Customers/"+ doc));
            //Pass Document Name and FileFormat of Document as Parameter in SaveToFile Method  
            convertdoc.SaveToFile(Server.MapPath("~/Invoices_To_Customers/"+ doc2), FileFormat.PDF);
            string messageSubject = "Items Requests(Kuweza)";
            string body = String.Format(@"<html>
                 <head>
               <b>SEE ATTACHED FILE FOR THE QUOTE</b>
                </head>              
                <body>
                <p>{0}</p>
                <p>[Thank you for doing business with us]</p>
                <tbody>
                ", CustomMessage);
            //Send attachment
            try
            {
                string docPath = Server.MapPath("~/Invoices_To_Customers/"+ doc2);
                if (CCEmail == null | CCEmail == "")
                {

                    var recipients = new List<string>();
                    recipients.Add(CompanyName + "<" + MainEmail + ">");
                    var fromAddress = new MailAddress("salebot@saleboat.co.za", "Items List From Kuweza");
                    _emailSender.SendMessage(messageSubject, fromAddress, body, recipients, docPath);
                }
                else
                {
                    var recipients = new List<string>();
                    recipients.Add(CompanyName + "<" + MainEmail + ">");
                    recipients.Add(CompanyName + "< " + CCEmail + ">");
                    var fromAddress = new MailAddress("salebot@saleboat.co.za", "Items List From Kuweza");
                    _emailSender.SendMessage(messageSubject, fromAddress, body, recipients, docPath);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
    }
}