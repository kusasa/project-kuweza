﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kuweza.Models;
using System.Collections;

namespace Kuweza.Controllers
{
    public class ItemsToSuppliersController : Controller
    {
        private KuwezaContext db = new KuwezaContext();
        SageConnectBase validate = new SageConnectBase();
        //Add Item From  Ajax Call During Drag And Drop
        public ActionResult AddItem(int ID, string RandomId, int ProductID, string ProductName, int SupplierId, string JobNumber, int Quantity, int CustomerId)
        {
            ItemsToSupplier itemsList = new ItemsToSupplier();
            ItemsToSupplier item = db.ItemsToSuppliers.FirstOrDefault(i => i.JobNumber == JobNumber && i.ProductID == ProductID && i.SupplierId==SupplierId);
            if (item == null)
            {
                itemsList.ID = ID;
                itemsList.RandomId = RandomId;
                itemsList.ProductID = ProductID;
                itemsList.ProductName = ProductName;
                itemsList.SupplierId = SupplierId;
                itemsList.JobNumber = JobNumber;
                itemsList.Quantity = Quantity;
                itemsList.CustomerId = CustomerId;
                db.ItemsToSuppliers.Add(itemsList);
                db.SaveChanges();
            }
            return null;
        }
        //Update Quote

        public ActionResult UpdateQuote(string JobNumber, int ClientId, string SalesId, string ReqBy, string Validity, string Sdate, string Delivery, string Project, string Ship, string DdpPoint, string Terms)
        {
            Quotation quoteDetails = new Quotation();
            Quotation item = db.Quotations.FirstOrDefault(i => i.JobNumber == JobNumber);
            if (item == null)
            {
                //Create
                quoteDetails.JobNumber = JobNumber;
                quoteDetails.CustomerId = ClientId;
                quoteDetails.SalesId = SalesId;
                quoteDetails.RequestedBy = ReqBy;
                quoteDetails.Validity = Validity;
                quoteDetails.ShipDate = Sdate;
                quoteDetails.ShipVia = Ship;
                quoteDetails.Delivery = Delivery;
                quoteDetails.Project = Project;
                quoteDetails.DdpPoint = DdpPoint;
                quoteDetails.Terms = Terms;
                db.Quotations.Add(quoteDetails);
                db.SaveChanges();
            }
            else
            if (item != null){
                //Update now
                item.JobNumber = JobNumber;
                item.CustomerId = ClientId;
                item.SalesId = SalesId;
                item.RequestedBy = ReqBy;
                item.Validity = Validity;
                item.ShipDate = Sdate;
                item.ShipVia = Ship;
                item.Delivery = Delivery;
                item.Project = Project;
                item.DdpPoint = DdpPoint;
                item.Terms = Terms;
                db.SaveChanges();
            }
            return null;
        }
        //Update  Item From  Ajax Call: Item status can be 1 0r 0, 1 means its selected to final list and 0 means its not yet on final list
        public ActionResult UpdateItemStatus(int ID)
        {
            ItemsToSupplier itemsToSupplier = db.ItemsToSuppliers.Find(ID);
            itemsToSupplier.Status = 1;
            db.SaveChanges();
            return null;
        }
        //Update  Item From  Ajax Call: Undo add item to list
        public ActionResult UndoItemAdd(int ID)
        {
            ItemsToSupplier itemsToSupplier = db.ItemsToSuppliers.Find(ID);
            itemsToSupplier.Status = 0;
            db.SaveChanges();
            return null;
        }
        public ActionResult GetFinalItems(int CustomerId, string JobNumber)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            List<ItemsToSupplier> allItems = db.ItemsToSuppliers.Where(i =>i.Status==1 && i.JobNumber==JobNumber && i.CustomerId==CustomerId).ToList();
            string listItems = "";
            foreach (ItemsToSupplier items in allItems)
            {
                var suppliers = api.SupplierRequest.Get(items.SupplierId);
                listItems += String.Format("<div class='row rowHover'  id='finalItemsDiv'><div class='col-lg-4 col-md-4'>{0}</div><div class='col-lg-4 col-md-4'>{1}</div><div class='col-lg-1 col-md-1'>{2}</div><div class='col-lg-1 col-md-1'>{3}</div><a href='#'><div class='col-lg-1 col-md-1' id={4} onclick='undoItemAdd(this.id)'><font color='#FF0000'><span class='glyphicon glyphicon-remove-circle'/></font></div></a></div><hr>", suppliers.Name, items.ProductName, items.Quantity, items.MarkUpCost, items.ID);
            }
            string data = String.Format(@"<div class='row alert alert-info'>
                <div class='col-lg-4 col-md-4'><b><u>Co.</u></b></div>
                <div class='col-lg-4 col-md-4'><b><u>Item</u></b></div>
                <div class='col-lg-1 col-md-4'><b><u>Qty</u></b></div>
                <div class='col-lg-1 col-md-4'><b><u>Cost/Item</u></b></div>
                </div>            
                {0}", listItems);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetModalItems()
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            List<ItemsToSupplier> allItems = db.ItemsToSuppliers.Where(i => i.Status == 1).ToList();
            string listItems = "";
            foreach (ItemsToSupplier items in allItems)
            {
                var suppliers = api.SupplierRequest.Get(items.SupplierId);
listItems += String.Format("<div class='row rowHover'><div class='col-lg-8'>{0}</div><div class='col-lg-2'>{1}</div><div class='col-lg-2'>{2}</div></div>", items.ProductName, items.Quantity, items.Cost);
            }
            string data = String.Format(@"<div class='row alert alert-info'>
                <div class='col-lg-8 col-md-8'><b><u>Item Name</u></b></div>
                <div class='col-lg-2 col-md-2'><b><u>Qty</u></b></div>
                <div class='col-lg-2 col-md-2'><b><u>Cost/Item</u></b></div>
                </div>            
                {0}", listItems);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Delete item from Ajax Call During Drag And Drop
        public ActionResult DeleteItem(string RandomID)
        {
            //ItemsToSupplier itemsDelete= db.ItemsToSuppliers.Find(RandomID);
            ItemsToSupplier  item = db.ItemsToSuppliers.FirstOrDefault(i => i.RandomId == RandomID);
            db.ItemsToSuppliers.Remove(item);
            db.SaveChanges();
            return null;
        }
        //Get json results and send them back to form
        public ActionResult LoadCompanies(int CustomerId, string JobNumber)
        {
            var api = new ApiRequest(validate.Username, validate.Password, validate.Apikey, validate.CompanyId);
            //I edited to look like below
            List<ItemsToSupplier> allItems = db.ItemsToSuppliers.GroupBy(i => i.SupplierId).Select(i => i.FirstOrDefault()).Where(i =>i.JobNumber == JobNumber && i.CustomerId == CustomerId).ToList();
            string listItems = "";
             
            foreach (ItemsToSupplier items in allItems)
             {
                var suppliers = api.SupplierRequest.Get(items.SupplierId);
                listItems += String.Format("<div class='row'><div class='col-lg-8' style='cursor:pointer;' id={0} draggable='true' ondragstart='dragstart(event)' onclick='clickstart(this.id)'>{1}</div><div class='col-lg-2' style='cursor:pointer;' onclick='downloadFile({0})'><span class='glyphicon glyphicon-download-alt'/></div><div class='col-lg-2' style='cursor:pointer;' onclick='openProofOfQuoteModal({0})'><span class='glyphicon glyphicon-plus'/></div></div><hr>", items.SupplierId, suppliers.Name);
             }
            string data = String.Format(@"<div class='row alert alert-info'>
                <div class='col-lg-8 col-md-8'><b><u>Supplier List</u></b></div>  <div class='col-lg-4 col-md-4'><b><u>Items Doc</u></b></div>             
                </div>            
                {0}", listItems);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetItems(int SupplierId, string JobNumber)
        {
            List<ItemsToSupplier> allItems = db.ItemsToSuppliers.Where(i => i.SupplierId == SupplierId && i.JobNumber==JobNumber).ToList();
            string listItems = "";
            foreach (ItemsToSupplier items in allItems)
            {
                listItems += String.Format("<div class='row rowHover'><div class='col-lg-4'>{0}</div><div class='col-lg-2'>{1}</div><div class='col-lg-2'>{2}</div><a href='#'><div class='col-lg-2' id={3} onclick='updateProduct(this.id)'><span class='glyphicon glyphicon-pencil'/></div></a><a href='#'><div class='col-lg-2' id={4} onclick='addToList(this.id)'><font color='#4285F4'><span class='glyphicon glyphicon-plus'/></font></div></a></div><hr>", items.ProductName, items.Quantity, items.Cost, items.ID, items.ID);
            }
            string data = String.Format(@"{0}", listItems);          
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetQuoteDetails(string JobNumber)
        {
            Quotation item = db.Quotations.FirstOrDefault(i => i.JobNumber == JobNumber);
            if (item != null)
            {
                string SalesId = item.SalesId;
                string RequestedBy = item.RequestedBy;
                string Validity = item.Validity;
                string ShipDate = item.ShipDate;
                string ShipVia = item.ShipVia;
                string Delivery = item.Delivery;
                string Project = item.Project;
                string DdpPoint = item.DdpPoint;
                string Terms = item.Terms;
                return Json(new { SalesId = SalesId, RequestedBy = RequestedBy, Validity = Validity, ShipVia = ShipVia, ShipDate = ShipDate, Delivery = Delivery, Project = Project, DdpPoint = DdpPoint, Terms = Terms });

            }
            else {

                return null;
            }
        }
        [HttpPost]
        public ActionResult GetItemsUpdate(int ID)
        {
            ItemsToSupplier item = db.ItemsToSuppliers.FirstOrDefault(i => i.ID == ID);
            double qty = item.Quantity;
            double costPerItem = item.Cost;
            double width = item.Width;
            double height = item.Heigth;
            double weight = item.Weight;
            double length = item.Length;
            string size = item.Size;
            double markup = item.MarkUp;
            double markUpCost = item.MarkUpCost;
            string deliveryDate = item.DeliveryDate;
            return Json(new { qty = qty, costPerItem = costPerItem, width = width, height = height, weight = weight, length = length, size = size,  deliveryDate = deliveryDate, markup = markup, markUpCost = markUpCost });
        }
        public ActionResult UpdateItem(int ID, double Cost, int Quantity, double TotalCost, string DeliveryDate, double Height, double Width,  double Weight, double Lngth, string Size, double MarkUp, double MarkUpCost)
        {
            ItemsToSupplier itemsToSupplier = db.ItemsToSuppliers.Find(ID);
            itemsToSupplier.Quantity = Quantity;
            itemsToSupplier.Cost = Cost;
            itemsToSupplier.TotalCost = TotalCost;
            itemsToSupplier.DeliveryDate = DeliveryDate;
            itemsToSupplier.Heigth = Height;
            itemsToSupplier.Width = Width;
            itemsToSupplier.Length = Lngth;
            itemsToSupplier.Size = Size;
            itemsToSupplier.Weight = Weight;
            itemsToSupplier.MarkUp = MarkUp;
            itemsToSupplier.MarkUpCost = MarkUpCost;
            db.SaveChanges();
            return null;
        }
        public ActionResult Index()
        {
            return View(db.ItemsToSuppliers.ToList());
        }
        public ActionResult _ProductsList()
        {
            ItemsToSupplier item = new ItemsToSupplier();
            return View(db.ItemsToSuppliers.ToList().Distinct());
        }
    
        public ActionResult _SupplierList()
        {
            return View(db.ItemsToSuppliers.ToList());
        }
        // GET: ItemsToSuppliers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemsToSupplier itemsToSupplier = db.ItemsToSuppliers.Find(id);
            if (itemsToSupplier == null)
            {
                return HttpNotFound();
            }
            return View(itemsToSupplier);
        }

        // GET: ItemsToSuppliers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ItemsToSuppliers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,RandomId,ProductID,ProductName,SupplierId,JobNumber")] ItemsToSupplier itemsToSupplier)
        {
            if (ModelState.IsValid)
            {
                db.ItemsToSuppliers.Add(itemsToSupplier);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(itemsToSupplier);
        }

        // GET: ItemsToSuppliers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemsToSupplier itemsToSupplier = db.ItemsToSuppliers.Find(id);
            if (itemsToSupplier == null)
            {
                return HttpNotFound();
            }
            return View(itemsToSupplier);
        }

        // POST: ItemsToSuppliers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,RandomId,ProductID,ProductName,SupplierId,JobNumber")] ItemsToSupplier itemsToSupplier)
        {
            if (ModelState.IsValid)
            {
                db.Entry(itemsToSupplier).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(itemsToSupplier);
        }

        // GET: ItemsToSuppliers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ItemsToSupplier itemsToSupplier = db.ItemsToSuppliers.Find(id);
            if (itemsToSupplier == null)
            {
                return HttpNotFound();
            }
            return View(itemsToSupplier);
        }

        // POST: ItemsToSuppliers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ItemsToSupplier itemsToSupplier = db.ItemsToSuppliers.Find(id);
            db.ItemsToSuppliers.Remove(itemsToSupplier);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
