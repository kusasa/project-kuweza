﻿using Kuweza.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kuweza.Controllers
{
    public class PaymentsDeliveriesController : Controller
    {
        private KuwezaContext db = new KuwezaContext();
        // GET: PaymentsDeliveries
        public ActionResult Index()
        {
            return View();
        }
        //Get json results and send them back to form
        public ActionResult GetSelectedItems(string JobNumber)
        {
            List<PaymentsDelivery> allItems = db.PaymentsDeliveries.Where(i => i.JobNumber==JobNumber).ToList();
            string listItems = "";
            foreach (PaymentsDelivery items in allItems)
            {
                listItems += String.Format("<div class='row'><div class='col-lg-8' style='cursor:pointer;' id={0} draggable='true' ondragstart='dragstart(event)' onclick='clickstart(this.id)'>{1}</div><div class='col-lg-2' style='cursor:pointer;' onclick='downloadFile({0})'><span class='glyphicon glyphicon-download-alt'/></div><div class='col-lg-2' style='cursor:pointer;' onclick='openProofOfQuoteModal({0})'><span class='glyphicon glyphicon-plus'/></div></div><hr>", items.ProductID, items.ProductID);
            }
            string data = String.Format(@"<div class='row alert alert-info'>
                <div class='col-lg-8 col-md-8'><b><u>Supplier List</u></b></div>  <div class='col-lg-4 col-md-4'><b><u>Items Doc</u></b></div>             
                </div>            
                {0}", listItems);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Add(int ID)
        {
            PaymentsDelivery itemID= new PaymentsDelivery();
            itemID.ProductID = ID;
            db.PaymentsDeliveries.Add(itemID);
            db.SaveChanges();
            return null;
        }
        public ActionResult Delete(int ID)
        {
            PaymentsDelivery itemID = db.PaymentsDeliveries.FirstOrDefault(x => x.ProductID == ID);
            db.PaymentsDeliveries.Remove(itemID);
            db.SaveChanges();
            return null;
        }
    }
}