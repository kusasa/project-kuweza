﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Kuweza.Models;
using System.IO;
using Spire.Doc;
using Spire.Doc.Documents;
using System;
using System.Collections.Generic;
using Spire.Doc.Fields;
using System.Drawing;

namespace Kuweza.Controllers
{
    public class InvoicesController : Controller
    {
        private KuwezaContext db = new KuwezaContext();

        // GET: Invoices
        public ActionResult Index()
        {
            return View(db.Invoices.ToList());
        }
        public ActionResult ViewInvoiceFormats()
        {
            return View(db.Invoices.ToList());
        }
       
        //Create new word doc for invoice format. then saves the file name to the database and also upload it to the invoicesformats folder
        public ActionResult ImportFile(string fileName, string invoiceCat)
        {
            Invoice invoicedocs = new Invoice();

            try
            {
                invoicedocs.InvoiceName = fileName;
                invoicedocs.InvoiceCat = invoiceCat;
                db.Invoices.Add(invoicedocs);
                db.SaveChanges();
                //Create word document
                Document document = new Document();
                //Create a new section
                Section section = document.AddSection();
                //Create a new paragraph
                Paragraph paragraph = section.AddParagraph();
                //Append Text
                paragraph.AppendText("THIS IS A BLANK DOC. STRUCTURE OR PASTE YOUR TEMPLATE FORMAT");
                //Save doc file.
                document.SaveToFile(Path.Combine(Server.MapPath("~/InvoicesFormats/"), fileName), FileFormat.Docx);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
            }
            return null;
        }
        //Open selected invoice
        public ActionResult OpenSelectedInvoice(int InvoiceID, int CompanyID, string JobNumber)
        {     
            try
            {
                // 
                Invoice inv = db.Invoices.Find(InvoiceID);
                string invoicename = inv.InvoiceName;
                string copyfrom = Path.Combine(Server.MapPath("~/InvoicesFormats/"), invoicename);
                string saveas = JobNumber + "C" + CompanyID + "Quote.docx";
                string saveto = Path.Combine(Server.MapPath("~/Quotes_To_Suppliers/"), saveas);
               // System.Diagnostics.Process.Start(Path.Combine(Server.MapPath("~/Quotes_To_Suppliers/"), saveas));
                try
                {
                    Document sourceDoc = new Document(copyfrom);
                    Document destinationDoc = new Document();
                    Section section = destinationDoc.AddSection();
                    //Create a new paragraph
                    Paragraph paragraph = section.AddParagraph();
                    //Append Text
                    paragraph.AppendText(".");
                    //Save doc file.
                    destinationDoc.SaveToFile(saveto, FileFormat.Docx);

                    foreach (Section sec in sourceDoc.Sections)
                    {
                        foreach (DocumentObject obj in sec.Body.ChildObjects)
                        {
                            destinationDoc.Sections[0].Body.ChildObjects.Add(obj.Clone());
                        }
                    }
                    destinationDoc.SaveToFile(saveto, FileFormat.Docx);
                    System.Diagnostics.Process.Start(Path.Combine(Server.MapPath("~/Quotes_To_Suppliers/"), saveas));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
            }
            return null;
        }
        //Open selected invoice
        public ActionResult OpenSelectedFinalInvoice(int InvoiceID, int CompanyID, string JobNumber)
        {
            try
            {
                // 
                Invoice inv = db.Invoices.Find(InvoiceID);
                string invoicename = inv.InvoiceName;
                string copyfrom = Path.Combine(Server.MapPath("~/InvoicesFormats/"), invoicename);
                string saveas = JobNumber + "C" + CompanyID + "Invoice.docx";
                string saveto = Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas);
                // System.Diagnostics.Process.Start(Path.Combine(Server.MapPath("~/Quotes_To_Suppliers/"), saveas));
                try
                {
                    Document sourceDoc = new Document(copyfrom);
                    Document destinationDoc = new Document();
                    Section section = destinationDoc.AddSection();
                    //Create a new paragraph
                    Paragraph paragraph = section.AddParagraph();
                    //Append Text
                    paragraph.AppendText(" ");
                    //Save doc file.
                    destinationDoc.SaveToFile(saveto, FileFormat.Docx);

                    foreach (Section sec in sourceDoc.Sections)
                    {
                        foreach (DocumentObject obj in sec.Body.ChildObjects)
                        {
                            destinationDoc.Sections[0].Body.ChildObjects.Add(obj.Clone());
                        }
                    }
                    destinationDoc.SaveToFile(saveto, FileFormat.Docx);
                    System.Diagnostics.Process.Start(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
            }
            return null;
        }
        //Open selected invoice
        public ActionResult InsertItems(int InvoiceID, int CompanyID, string JobNumber)
        {
            try
            {
                // 
                Invoice inv = db.Invoices.Find(InvoiceID);
                string invoicename = inv.InvoiceName;
                string saveas = JobNumber + "C" + CompanyID + "Quote.docx";
                try
                {
                    List<ItemsToSupplier> allItems = db.ItemsToSuppliers.Where(i => i.SupplierId == CompanyID && i.Status == 0 && i.JobNumber==JobNumber).ToList();
                    //Find elements in list
                    int count = allItems.Count();
                    //Now we know number of items lets call the document to populate iteams
                    Document document = new Document();
                    document.LoadFromFile(Path.Combine(Server.MapPath("~/Quotes_To_Suppliers/"), saveas));
                    Section sect = document.Sections[0];
                    TextSelection selection = document.FindString("P*TL0*01", true, true);
                    TextRange range = selection.GetAsOneRange();
                    Paragraph para = range.OwnerParagraph;
                    Body body = para.OwnerTextBody;
                    int index = body.ChildObjects.IndexOf(para);

                    //***************/ Initialise a tabvle

                    Table table = sect.AddTable(true);
                    String[] header = { "Description", "Quantity"};
                    int x = 0;
                    String[][] data = new String[count][];
                    foreach (ItemsToSupplier items in allItems)
                    {
                        data[x] = new String[2];
                        data[x][0] = items.ProductName;
                        data[x][1] = Convert.ToString(items.Quantity);
                        x++;
                    }
                    table.ResetCells(data.Length + 1, header.Length);
                    // Create rows
                    TableRow Frow = table.Rows[0];
                    Frow.IsHeader = true;
                    Frow.Height = 25;    //unit: point, 1point = 0.3528 mm
                    Frow.HeightType = TableRowHeightType.Exactly;
                    Frow.RowFormat.BackColor = Color.Pink;
                    for (int i = 0; i < header.Length; i++)
                    {
                        Frow.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                        Paragraph p = Frow.Cells[i].AddParagraph();
                        p.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Left;
                        TextRange txtRange = p.AppendText(header[i]);
                        txtRange.CharacterFormat.Bold = true;
                    }
                    for (int r = 0; r < data.Length; r++)
                    {
                        TableRow dataRow = table.Rows[r + 1];
                        dataRow.Height = 20;
                        dataRow.HeightType = TableRowHeightType.Exactly;
                        dataRow.RowFormat.BackColor = Color.Empty;
                        for (int c = 0; c < data[r].Length; c++)
                        {
                            dataRow.Cells[c].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                            dataRow.Cells[c].AddParagraph().AppendText(data[r][c]);
                        }
                    }
                    //***********
                    body.ChildObjects.Remove(para);
                    body.ChildObjects.Insert(index, table);
                    //Save inserted table
                    document.SaveToFile(Path.Combine(Server.MapPath("~/Quotes_To_Suppliers/"), saveas));
                    //Open document
                    System.Diagnostics.Process.Start(Path.Combine(Server.MapPath("~/Quotes_To_Suppliers/"), saveas));
                
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                // Create another table in top of the items list
               
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
            }
            return null;
        }
        //Open selected invoice
        public ActionResult InsertFinalItems(int InvoiceID, int CompanyID, string JobNumber)
        {
            try
            {
                // 
                Invoice inv = db.Invoices.Find(InvoiceID);
                string invoicename = inv.InvoiceName;
                string saveas = JobNumber + "C" + CompanyID + "Invoice.docx";
                try
                {
                    List<ItemsToSupplier> allItems = db.ItemsToSuppliers.Where(i => i.JobNumber == JobNumber && i.CustomerId == CompanyID && i.Status==1).ToList();
                    //Find elements in list
                    int count = allItems.Count();
                    //Now we know number of items lets call the document to populate iteams
                    Document document = new Document();
                    document.LoadFromFile(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas));
                    Section sect = document.Sections[0];
                    TextSelection selection = document.FindString("P*TL0*01", true, true);
                    TextRange range = selection.GetAsOneRange();
                    Paragraph para = range.OwnerParagraph;

                    Body body = para.OwnerTextBody;
                    int index = body.ChildObjects.IndexOf(para);

                    //***************/ Initialise a tabvle

                    Table table = sect.AddTable(true);
                    String[] header = { "ITEMS", "QTY", "DESCRIPTION", "UNIT PRICE", "AMOUNT" };
                    int x = 0;
                    int y = 0;
                    String[][] data = new String[count][];
                    foreach (ItemsToSupplier items in allItems) { 

                        data[x] = new String[5];
                        data[x][0] = Convert.ToString(++y);
                        data[x][1] = Convert.ToString(items.Quantity); 
                        data[x][2] = items.ProductName;
                        data[x][3] = Convert.ToString(items.Cost);
                        data[x][4] = Convert.ToString(items.TotalCost);
                        x++;
                    }
                    table.ResetCells(data.Length + 1, header.Length);
                    // Create rows
                    TableRow Frow = table.Rows[0];
                    Frow.IsHeader = true;
                    Frow.Height = 25;   //unit: point, 1point = 0.3528 mm
                    Frow.HeightType = TableRowHeightType.Exactly;
                    Frow.RowFormat.BackColor = Color.Pink;
                    for (int i = 0; i < header.Length; i++)
                    {
                        Frow.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                        Paragraph p = Frow.Cells[i].AddParagraph();
                        p.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Left;
                        TextRange txtRange = p.AppendText(header[i]);
                        txtRange.CharacterFormat.Bold = true;
                        txtRange.CharacterFormat.FontName = "Calibri";
                        txtRange.CharacterFormat.FontSize = 8;
                    }
                    for (int r = 0; r < data.Length; r++)
                    {
                        TableRow dataRow = table.Rows[r + 1];
                        dataRow.Height = 20;
                        dataRow.HeightType = TableRowHeightType.Exactly;
                        dataRow.RowFormat.BackColor = Color.Empty;
                        for (int c = 0; c < data[r].Length; c++)
                        {
                            dataRow.Cells[c].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                            TextRange TR= dataRow.Cells[c].AddParagraph().AppendText(data[r][c]);
                            TR.CharacterFormat.FontName = "Calibri";
                            TR.CharacterFormat.FontSize = 10;
                        }
                    }
                   

                    //***********
                    body.ChildObjects.Remove(para);
                    body.ChildObjects.Insert(index, table);
                    //Save inserted table
                    document.SaveToFile(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas));
                    //Open document
                  

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                try
                {
                    //Create word document
                    Document document = new Document();
                    //Create a new section
                    document.LoadFromFile(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas));
                   // Section section = document.AddSection();
                    //Create a new paragraph
                    //Paragraph paragraph = section.AddParagraph();
                    //Append Text
                    Paragraph paraInserted = document.Sections[0].AddParagraph();
                    TextRange textRange1 = paraInserted.AppendText("P*TL0*01");
                    document.Sections[0].Paragraphs.Insert(0, document.Sections[0].Paragraphs[document.Sections[0].Paragraphs.Count - 1]);                
                    //Save doc file.
                    document.SaveToFile(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas), FileFormat.Docx);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error:" + ex.Message);
                }
                //And top table to the document
                try
                {
                    List<Quotation> allItems = db.Quotations.Where(i => i.JobNumber == JobNumber).ToList();
                    //Find elements in list
                    int count = allItems.Count();
                    //Now we know number of items lets call the document to populate iteams
                    Document document = new Document();
                    document.LoadFromFile(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas));
                    Section sect = document.Sections[0];
                    TextSelection selection = document.FindString("P*TL0*01", true, true);
                    TextRange range = selection.GetAsOneRange();
                    Paragraph para = range.OwnerParagraph;

                    Body body = para.OwnerTextBody;
                    int index = body.ChildObjects.IndexOf(para);

                    //***************/ Initialise a tabvle

                    Table table = sect.AddTable(true);
                    String[] header = { "SALES ID", "REQ. BY", "VALIDITY", "SHIP DATE", "DELIVERY", "PROJECT", "SHIP VIA", "C&F POINT", "TERMS" };
                    int x = 0;               
                    String[][] data = new String[count][];
                    foreach (Quotation items in allItems)
                    {
                        data[x] = new String[9];
                        data[x][0] = items.SalesId;
                        data[x][1] = items.RequestedBy;
                        data[x][2] = items.Validity;
                        data[x][3] = items.ShipDate;
                        data[x][4] = items.Delivery;
                        data[x][5] = items.Project;
                        data[x][6] = items.ShipVia;
                        data[x][7] = items.DdpPoint;
                        data[x][8] = items.Terms;             
                        x++;
                    }
                    table.ResetCells(data.Length + 1, header.Length);
                    // Create rows
                    TableRow Frow = table.Rows[0];
                    Frow.IsHeader = true;
                    Frow.Height = 25;   //unit: point, 1point = 0.3528 mm
                    Frow.HeightType = TableRowHeightType.Exactly;
                    Frow.RowFormat.BackColor = Color.Pink;
                    for (int i = 0; i < header.Length; i++)
                    {
                        Frow.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                        Paragraph p = Frow.Cells[i].AddParagraph();
                        p.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Left;
                        TextRange txtRange = p.AppendText(header[i]);
                        txtRange.CharacterFormat.Bold = true;
                        txtRange.CharacterFormat.FontName = "Calibri";
                        txtRange.CharacterFormat.FontSize = 8;
                    }
                    for (int r = 0; r < data.Length; r++)
                    {
                        TableRow dataRow = table.Rows[r + 1];
                        dataRow.Height = 20;
                        dataRow.HeightType = TableRowHeightType.Exactly;
                        dataRow.RowFormat.BackColor = Color.Empty;
                        for (int c = 0; c < data[r].Length; c++)
                        {
                            dataRow.Cells[c].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                            TextRange TR = dataRow.Cells[c].AddParagraph().AppendText(data[r][c]);
                            TR.CharacterFormat.FontName = "Calibri";
                            TR.CharacterFormat.FontSize = 10;
                        }
                    }
                    //***********
                    body.ChildObjects.Remove(para);
                    body.ChildObjects.Insert(index, table);
                    //Save inserted table
                    document.SaveToFile(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas));
                    //Open document
                    System.Diagnostics.Process.Start(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                //End of the top table to the document
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
            }
            return null;
        }
        //Open invoice format on drop
        public ActionResult OpenInvoiceSampleFile(string fileName)
        {
            char[] charsToTrim = { ' ', '\t' };
            string fname = fileName.Trim(charsToTrim);
            try
            {
                //Open File.
                System.Diagnostics.Process.Start(Path.Combine(Server.MapPath("~/InvoicesFormats/"), fname));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
            }
            return null;
        }
        //Open invoice format on drop
        public ActionResult OpenSampleFile(int id)
        {
            Invoice item = db.Invoices.FirstOrDefault(i => i.InvoiceId == id);
            string fileName = item.InvoiceName;
            char[] charsToTrim = { ' ', '\t' };
            string fname = fileName.Trim(charsToTrim);
            try
            {
                //Open File.
                System.Diagnostics.Process.Start(Path.Combine(Server.MapPath("~/InvoicesFormats/"), fname));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
            }
            return null;
        }
        //Get Invoice Formats
        public ActionResult GetInvoiceFormats()
        {
            List<Invoice> allItems = db.Invoices.ToList();
            string listItems = "";
            foreach (Invoice items in allItems)
            {
                listItems += String.Format("<div class='row'><div class ='col-sm-4' id={1} draggable='true' ondragstart='dragstart(event)'>{0}</div><div class ='col-sm-8' id={1}  style='cursor:pointer;' onclick = 'openSampleDoc(this.id)'>{2}</div></div>", items.InvoiceCat, items.InvoiceId, items.InvoiceName);
            }
            string data = String.Format(@"{0}", listItems);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Delete doc sample
        public ActionResult DeleteSampleFile(int id)
        {
            Invoice item = db.Invoices.FirstOrDefault(i => i.InvoiceId == id);
            string fileName = item.InvoiceName;
            char[] charsToTrim = { ' ', '\t' };
            string fname = fileName.Trim(charsToTrim);
            string filePath = Path.Combine(Server.MapPath("~/InvoicesFormats/"), fname);
            if (System.IO.File.Exists(filePath))
            {
                db.Invoices.Remove(item);
                db.SaveChanges();
                System.IO.File.Delete(filePath);
            }
            return null;
        }
        //Get Invoice Formats
        public ActionResult SelectInvoiceFormats()
        {
            List<Invoice> allItems = db.Invoices.ToList();
            string listItems = "";
            foreach (Invoice items in allItems)
            {
                listItems += String.Format("<div class='row' id={0} onclick='selectQuote(this.id)'>{1}</div>", items.InvoiceId, items.InvoiceName);
            }
            string data = String.Format(@"{0}", listItems);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SelectFinalInvoiceFormats()
        {
            List<Invoice> allItems = db.Invoices.ToList();
            string listItems = "";
            foreach (Invoice items in allItems)
            {
                listItems += String.Format("<div class='row' id={0} onclick='selectFinalQuote(this.id)'>{1}</div>", items.InvoiceId, items.InvoiceName);
            }
            string data = String.Format(@"{0}", listItems);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //Open selected invoice
        public ActionResult SwipFormat(int InvoiceID, int CompanyID, string JobNumber)
        {
            try
            {
                // 
                Invoice inv = db.Invoices.Find(InvoiceID);
                string invoicename = inv.InvoiceName;
                string saveas = JobNumber + "C" + CompanyID + "Invoice.docx";
                try
                {
                    List<ItemsToSupplier> allItems = db.ItemsToSuppliers.Where(i => i.JobNumber == JobNumber && i.CustomerId == CompanyID && i.Status == 1).ToList();
                    //Find elements in list
                    int count = allItems.Count();
                    //Now we know number of items lets call the document to populate iteams
                    Document document = new Document();
                    document.LoadFromFile(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas));
                    Section sect = document.Sections[0];
                    TextSelection selection = document.FindString("P*TL0*01", true, true);
                    TextRange range = selection.GetAsOneRange();
                    Paragraph para = range.OwnerParagraph;

                    Body body = para.OwnerTextBody;
                    int index = body.ChildObjects.IndexOf(para);

                    //***************/ Initialise a tabvle

                    Table table = sect.AddTable(true);
                    String[] header = { "ITEMS", "QTY", "DESCRIPTION", "UNIT PRICE", "AMOUNT" };
                    int x = 0;
                    int y = 0;
                    String[][] data = new String[count][];
                    foreach (ItemsToSupplier items in allItems)
                    {

                        data[x] = new String[5];
                        data[x][0] = Convert.ToString(++y);
                        data[x][1] = Convert.ToString(items.Quantity);
                        data[x][2] = items.ProductName;
                        data[x][3] = Convert.ToString(items.Cost);
                        data[x][4] = Convert.ToString(items.TotalCost);
                        x++;
                    }
                    table.ResetCells(data.Length + 1, header.Length);
                    // Create rows
                    TableRow Frow = table.Rows[0];
                    Frow.IsHeader = true;
                    Frow.Height = 25;   //unit: point, 1point = 0.3528 mm
                    Frow.HeightType = TableRowHeightType.Exactly;
                    Frow.RowFormat.BackColor = Color.Pink;
                    for (int i = 0; i < header.Length; i++)
                    {
                        Frow.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                        Paragraph p = Frow.Cells[i].AddParagraph();
                        p.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Left;
                        TextRange txtRange = p.AppendText(header[i]);
                        txtRange.CharacterFormat.Bold = true;
                        txtRange.CharacterFormat.FontName = "Calibri";
                        txtRange.CharacterFormat.FontSize = 8;
                    }
                    for (int r = 0; r < data.Length; r++)
                    {
                        TableRow dataRow = table.Rows[r + 1];
                        dataRow.Height = 20;
                        dataRow.HeightType = TableRowHeightType.Exactly;
                        dataRow.RowFormat.BackColor = Color.Empty;
                        for (int c = 0; c < data[r].Length; c++)
                        {
                            dataRow.Cells[c].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                            TextRange TR = dataRow.Cells[c].AddParagraph().AppendText(data[r][c]);
                            TR.CharacterFormat.FontName = "Calibri";
                            TR.CharacterFormat.FontSize = 10;
                        }
                    }


                    //***********
                    body.ChildObjects.Remove(para);
                    body.ChildObjects.Insert(index, table);
                    //Save inserted table
                    document.SaveToFile(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas));
                    //Open document


                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                try
                {
                    //Create word document
                    Document document = new Document();
                    //Create a new section
                    document.LoadFromFile(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas));
                    // Section section = document.AddSection();
                    //Create a new paragraph
                    //Paragraph paragraph = section.AddParagraph();
                    //Append Text
                    Paragraph paraInserted = document.Sections[0].AddParagraph();
                    TextRange textRange1 = paraInserted.AppendText("P*TL0*01");
                    document.Sections[0].Paragraphs.Insert(0, document.Sections[0].Paragraphs[document.Sections[0].Paragraphs.Count - 1]);
                    //Save doc file.
                    document.SaveToFile(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas), FileFormat.Docx);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error:" + ex.Message);
                }
                //And top table to the document
                try
                {
                    List<Quotation> allItems = db.Quotations.Where(i => i.JobNumber == JobNumber).ToList();
                    //Find elements in list
                    int count = allItems.Count();
                    //Now we know number of items lets call the document to populate iteams
                    Document document = new Document();
                    document.LoadFromFile(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas));
                    Section sect = document.Sections[0];
                    TextSelection selection = document.FindString("P*TL0*01", true, true);
                    TextRange range = selection.GetAsOneRange();
                    Paragraph para = range.OwnerParagraph;

                    Body body = para.OwnerTextBody;
                    int index = body.ChildObjects.IndexOf(para);

                    //***************/ Initialise a tabvle

                    Table table = sect.AddTable(true);
                    String[] header = { "SALES ID", "REQ. BY", "VALIDITY", "SHIP DATE", "DELIVERY", "PROJECT", "SHIP VIA", "C&F POINT", "TERMS" };
                    int x = 0;
                    String[][] data = new String[count][];
                    foreach (Quotation items in allItems)
                    {
                        data[x] = new String[9];
                        data[x][0] = items.SalesId;
                        data[x][1] = items.RequestedBy;
                        data[x][2] = items.Validity;
                        data[x][3] = items.ShipDate;
                        data[x][4] = items.Delivery;
                        data[x][5] = items.Project;
                        data[x][6] = items.ShipVia;
                        data[x][7] = items.DdpPoint;
                        data[x][8] = items.Terms;
                        x++;
                    }
                    table.ResetCells(data.Length + 1, header.Length);
                    // Create rows
                    TableRow Frow = table.Rows[0];
                    Frow.IsHeader = true;
                    Frow.Height = 25;   //unit: point, 1point = 0.3528 mm
                    Frow.HeightType = TableRowHeightType.Exactly;
                    Frow.RowFormat.BackColor = Color.Pink;
                    for (int i = 0; i < header.Length; i++)
                    {
                        Frow.Cells[i].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                        Paragraph p = Frow.Cells[i].AddParagraph();
                        p.Format.HorizontalAlignment = Spire.Doc.Documents.HorizontalAlignment.Left;
                        TextRange txtRange = p.AppendText(header[i]);
                        txtRange.CharacterFormat.Bold = true;
                        txtRange.CharacterFormat.FontName = "Calibri";
                        txtRange.CharacterFormat.FontSize = 8;
                    }
                    for (int r = 0; r < data.Length; r++)
                    {
                        TableRow dataRow = table.Rows[r + 1];
                        dataRow.Height = 20;
                        dataRow.HeightType = TableRowHeightType.Exactly;
                        dataRow.RowFormat.BackColor = Color.Empty;
                        for (int c = 0; c < data[r].Length; c++)
                        {
                            dataRow.Cells[c].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                            TextRange TR = dataRow.Cells[c].AddParagraph().AppendText(data[r][c]);
                            TR.CharacterFormat.FontName = "Calibri";
                            TR.CharacterFormat.FontSize = 10;
                        }
                    }
                    //***********
                    body.ChildObjects.Remove(para);
                    body.ChildObjects.Insert(index, table);
                    //Save inserted table
                    document.SaveToFile(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas));
                    //Open document
                    System.Diagnostics.Process.Start(Path.Combine(Server.MapPath("~/Invoices_To_Customers/"), saveas));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                //End of the top table to the document
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error:" + ex.Message);
            }
            return null;
        }

        // GET: Invoices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // GET: Invoices/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Invoices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InvoiceId,InvoiceName")] Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                db.Invoices.Add(invoice);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(invoice);
        }

        // GET: Invoices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // POST: Invoices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InvoiceId,InvoiceName")] Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(invoice).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(invoice);
        }

        // GET: Invoices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // POST: Invoices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Invoice invoice = db.Invoices.Find(id);
            db.Invoices.Remove(invoice);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}