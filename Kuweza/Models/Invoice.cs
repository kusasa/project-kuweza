﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kuweza.Models
{
    public class Invoice
    {
        public int InvoiceId { get; set; }
        public string InvoiceName { get; set; }
        public string InvoiceCat { get; set; }
    }
}