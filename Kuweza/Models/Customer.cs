﻿using System.ComponentModel.DataAnnotations;

namespace Kuweza.Models
{
    public class Customer : BaseModel
    {
        public string Name { get; set; }
        //  public Category Category { get; set; }
      
        // public SalesRepresentative SalesRepresentative { get; set; }
       // public string TaxReference { get; set; }
        public string ContactName { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string WebAddress { get; set; }
        [Display(Name = "Business Reg#")]
        public string BusinessRegistrationNumber { get; set; }
        //  public bool Active { get; set; }
        //  public decimal Balance { get; private set; }
        //   public decimal CreditLimit { get; set; }
        //   public int CommunicationMethod { get; set; }
        [Display(Name = "Postal Address Place")]
        public string PostalAddress01 { get; set; }
        [Display(Name = "Postal Address Street")]
        public string PostalAddress02 { get; set; }
        [Display(Name = "Postal Address City")]
        public string PostalAddress03 { get; set; }
        [Display(Name = "Postal Address Country")]
        public string PostalAddress04 { get; set; }
        [Display(Name = "Postal Address Code")]
        public string PostalAddress05 { get; set; }
        [Display(Name = "Delivery Place")]
        public string DeliveryAddress01 { get; set; }
        [Display(Name = "Delivery Street")]
        public string DeliveryAddress02 { get; set; }
        [Display(Name = "Delivery City")]
        public string DeliveryAddress03 { get; set; }
        [Display(Name = "Delivery Country")]
        public string DeliveryAddress04 { get; set; }
        [Display(Name = "Delivery Area Code")]
        public string DeliveryAddress05 { get; set; }
        public int? SalesRepresentativeId { get; set; }
        [Display(Name = "Category")]
        public string TextField1 { get; set; }
        [Display(Name = "Markup(%)")]
        public string TextField2 { get; set; }
        //     public bool AutoAllocateToOldestInvoice { get; set; }
        //     public bool EnableCustomerZone { get; set; }
        //     public string CustomerZoneGuid { get; private set; }
        //     public bool CashSale { get; set; }
        //     public string TextField1 { get; set; }
        //     public string TextField2 { get; set; }
        //     public string TextField3 { get; set; }
        //     public decimal NumericField1 { get; set; }
        //    public decimal NumericField2 { get; set; }
        //     public decimal NumericField3 { get; set; }
        //     public bool YesNoField1 { get; set; }
        //     public bool YesNoField2 { get; set; }
        //     public bool YesNoField3 { get; set; }
        //     public string DateField1 { get; set; }
        //    public string DateField2 { get; set; }
        //    public string DateField3 { get; set; }
        //     public int DefaultPriceListId { get; set; }
        //  public DefaultPricelist DefaultPriceList { get; set; }
        //     public string DefaultPriceListName { get; set; }
        //      public bool AcceptsElectronicInvoices { get; set; }
        //      public string Modified { get; private set; }
        //      public string Created { get; private set; }

        //       public string TaxStatusVerified { get; set; }
        //     public int CurrencyId { get; set; }
    }
}