﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kuweza.Models
{
    public class Warehouse
    {
        public int ID { get; set; }
        public string WarehouseName { get; set; }
        public string Location { get; set; }
        public string OtherNotes { get; set; }
        public string DateCreated { get; set; }
    }
}