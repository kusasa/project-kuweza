﻿using System.Collections.Generic;

namespace Kuweza.Models
{
    public class PagingResponse<T>
    {
        public int TotalResults { get; set; }
        public int ReturnedResults { get; set; }
        public List<T> Results { get; set; }
    }
}