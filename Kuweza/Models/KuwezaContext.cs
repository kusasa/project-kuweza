﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kuweza.Models
{
    public class KuwezaContext : DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Job> Jobs { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Supplier> Suppliers { get; set; }

        public DbSet<Item> Items { get; set; }

        public DbSet<ItemsToSupplier> ItemsToSuppliers { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Invoice> Invoices { get; set; }

        public DbSet<Quotation> Quotations { get; set; }

        public DbSet<ProofOfSuppliersQuote> ProofOfSuppliersQuotes { get; set; }

        public DbSet<Warehouse> Warehouses { get; set; }

        public DbSet<PaymentsDelivery> PaymentsDeliveries { get; set; }
    }
}