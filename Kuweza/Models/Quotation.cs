﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kuweza.Models
{
    public class Quotation
    {
        public int ID { get; set; }
        public string JobNumber{ get; set; }
        public int CustomerId { get; set; }
        public string SalesId { get; set; }
        public string RequestedBy { get; set; }
        public string Project { get; set; }
        public string Validity { get; set; }
        public string ShipDate { get; set; }
        public string ShipVia { get; set; }
        public string Delivery { get; set; }
        public string DdpPoint { get; set; }
        public string Terms { get; set; }
    }
}