﻿using System.ComponentModel.DataAnnotations;

namespace Kuweza.Models
{
    public class Supplier : BaseModel
    {
        public string Name { get; set; }
        public Category Category { get; set; }
        public string TaxReference { get; set; }
        public string ContactName { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string WebAddress { get; set; }
        [Display(Name = "Postal Address Place")]
        public string PostalAddress01 { get; set; }
        [Display(Name = "Postal Address Street")]
        public string PostalAddress02 { get; set; }
        [Display(Name = "Postal Address City")]
        public string PostalAddress03 { get; set; }
        [Display(Name = "Postal Address Country")]
        public string PostalAddress04 { get; set; }
        [Display(Name = "Postal Address Code")]
        public string PostalAddress05 { get; set; }
        [Display(Name = "Delivery Place")]
        public string DeliveryAddress01 { get; set; }
        [Display(Name = "Delivery Street")]
        public string DeliveryAddress02 { get; set; }
        [Display(Name = "Delivery City")]
        public string DeliveryAddress03 { get; set; }
        [Display(Name = "Delivery Country")]
        public string DeliveryAddress04 { get; set; }
        [Display(Name = "Delivery Area Code")]
        public string DeliveryAddress05 { get; set; }
        [Display(Name = "Business Reg#")]
        public string BusinessRegistrationNumber { get; set; }
        public string RMCDApprovalNumber { get; set; }
        [Display(Name = "Category")]
        public string TextField1 { get; set; }
    }
}