﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kuweza.Models
{
    public class Product
    {
        public int ProductId { get; set; }
        public string JobNumber { get; set; }
        public int ClientId { get; set; }
        public string ProductName { get; set; }     
        public int Quantity { get; set; }
        public string RandomId { get; set; }
        public int SageProductId { get; set; }
    }
}
