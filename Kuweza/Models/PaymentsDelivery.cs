﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kuweza.Models
{
    public class PaymentsDelivery
    {
        public int ID { get; set; }
        public string RandomId { get; set; }
        public int ProductID { get; set; }
        public int SupplierId { get; set; }
        public int CustomerId { get; set; }
        public string JobNumber { get; set; }
        public int PaymentStatus { get; set; }
        public string DatePaid { get; set; }
        public string ProofOfPayment { get; set; }
        public double WarehouseId { get; set; }
        public int WarehouseStatus { get; set; }
        public string DateStored { get; set; }
        public string User { get; set; }
        public string DeliveryDate { get; set; }
    }
}