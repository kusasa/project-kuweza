﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace Kuweza.Models
{
    public class ProofOfSuppliersQuote
    {
        public int Id { get; set; }
        public string JobNumber { get; set; }
        public int SupplierId { get; set; }
        public string OtherInfor { get; set; }
        public string FileName { get; set; }
    }
}