﻿using System;

namespace Kuweza.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string Description { get; set; }
        public DateTime Modified { get; set; }
        public DateTime Created { get; set; }
    }
}