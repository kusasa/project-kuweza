﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kuweza.Models
{
    public class Item : BaseModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public Category Category { get; set; }
        public decimal PriceInclusive { get; set; }
        public decimal PriceExclusive { get; set; }
        [Display(Name = "Weight(Kg)")]
        public string TextUserField1 { get; set; }
        [Display(Name = "Lenght(M)")]
        public decimal NumericUserField1 { get; set; }
        [Display(Name = "Breath(M)")]
        public decimal NumericUserField2 { get; set; }
        [Display(Name = "Height(M)")]
        public decimal NumericUserField3 { get; set; }
        public bool Active { get; set; }
        [Display(Name = "Category")]
        public string TextUserField2 { get; set; }
    }
}