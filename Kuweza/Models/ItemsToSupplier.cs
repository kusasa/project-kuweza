﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kuweza.Models
{
    public class ItemsToSupplier
    {
        public int ID { get; set; }
        public string RandomId { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public int Quantity { get; set; }
        public int SupplierId { get; set; }
        public int CustomerId { get; set; }
        public string JobNumber { get; set; }
        public double Cost { get; set; }
        public double TotalCost { get; set; }
        public int Status { get; set; }
        public double MarkUp { get; set; }
        public double MarkUpCost { get; set; }
        public double Width { get; set; }
        public double Heigth { get; set; }
        public double Length { get; set; }
        public string Size { get; set; }
        public double Weight { get; set; }
        public string DeliveryDate { get; set; }
    }
}