﻿using System;

namespace Kuweza.Models
{
    public class Job
    {
        public int JobId { get; set; }
        public string JobNumber { get; set; }
        public int StatusId { get; set; }
        public string TypeOfEquiry { get; set; }
        public bool Quoted { get; set; }
        public DateTime DateReceived { get; set; }
        public string KuwezaQuoteNo { get; set; }
        public string ProofOfEnquiry { get; set; }
        public string MethodOfCommunication { get; set; }
        public string AssignTo { get; set; }
        public int ClientId { get; set; }
        public string UserId { get; set; }     
    }
}