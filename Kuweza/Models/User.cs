﻿using System.ComponentModel.DataAnnotations;
namespace Kuweza.Models
{
    public class User
    {
        [Display(Name = "User ID")]
        [Key]
        public string UserId { get; set; }
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }
        public string UserFName { get; set; }
        public string UserSName { get; set; }
        public string UserPhone { get; set; }
        public string UserEmail { get; set; }
        public string Role { get; set; }
    }
}