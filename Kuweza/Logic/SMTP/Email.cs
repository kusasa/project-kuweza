﻿using Kuweza.Interfaces;
using SendGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Kuweza.Logic.SMTP
{
    public class Email : IEmail
    {
        public void SendMessage(string subject, MailAddress from, string html, List<string> recipients, string attachment = null)
        {
            string API_KEY = System.Configuration.ConfigurationManager.AppSettings["sendgrid_api_key"];

            var myMessage = new SendGridMessage();

            // Add multiple addresses to the To field.
            //@"Jeff Smith <lawrence@kusasa.biz>",
            //@"Anna Lidman <craig@kusasa.biz>",
            //@"Peter Saddow <michael@kusasa.biz>"

            myMessage.AddTo(recipients);
            myMessage.Subject = subject;
            myMessage.From = from;

            myMessage.Html = html;
            if (attachment != null) myMessage.AddAttachment(attachment);

            // Create a Web transport, using API Key
            var transportWeb = new Web(API_KEY);

            // Send the email.
            transportWeb.DeliverAsync(myMessage);
        }
    }
}