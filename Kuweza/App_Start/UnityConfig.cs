﻿using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Kuweza.Controllers;
using Kuweza.Interfaces;
using Kuweza.Logic.SMTP;
using System.Data.Entity;
using Kuweza.Models;

namespace Kuweza
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            // register all your components with the container here
            // it is NOT necessary to register your controllers           
            // e.g. container.RegisterType<ITestService, TestService>();  
            container.RegisterType<IEmail, Email>();
            container.RegisterType<IController, ReportEmailerController>();
            container.RegisterType<DbContext, ApplicationDbContext>(new HierarchicalLifetimeManager());
           // DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            DependencyResolver.SetResolver(new Unity.Mvc5.UnityDependencyResolver(container));
        }
    }
}